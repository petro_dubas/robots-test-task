<?php

//load all classes:
require_once __DIR__.'/assets/factory.php';
require_once __DIR__.'/assets/robots.php';

use Robots\Factory;
use Robots\UnionRobot;

//init factory and add robot types
$factory = new Factory();
$factory->addType(new Robots\MyHydra1());
$factory->addType(new Robots\MyHydra2());
//$factory->showRobotTypes();

//create some robots:
echo '<pre><h3>Hydra1:</h3>';
var_dump($factory->createMyHydra1(5));
echo '<h3>Hydra2:</h3>';
var_dump($factory->createMyHydra2(2));
echo '</pre>';

//init union robots and add a few of them to unite
$unionRobot = new UnionRobot();
$unionRobot->addRobot(new Robots\MyHydra2());
$unionRobot->addRobot($factory->createMyHydra2(2));

//add unionRobots to factory and create one of them
$factory->addType($unionRobot);
$res = reset($factory->createUnionRobot(1));
echo '<pre><h3>Union Robot:</h3>';
var_dump($res);
var_dump($res->getSpeed());
var_dump($res->getWeight());
var_dump($res->getHeight());
echo '</pre>';

