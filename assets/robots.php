<?php

namespace Robots;

/*
 * Used just to have got somekind of skeleton
 */
class DefaultRobot
{
	public $weight;
	public $speed;
	public $height;
	public $typeName;

	/**
	 * Function used to return robot speed
	 *
	 * @return mixed
	 */
	public function getSpeed()
	{
		return $this->speed;
	}

	/**
	 * Function used to return robot weight
	 *
	 * @return mixed
	 */
	public function getWeight()
	{
		return $this->weight;
	}

	/**
	 * Function used to return robot height
	 *
	 * @return mixed
	 */
	public function getHeight()
	{
		return $this->height;
	}

}

/*
 * Classes that actually use the DefaultRobot skeleton
 */
class MyHydra1 extends DefaultRobot
{
	/**
	 * MyHydra1 constructor.
	 */
	public function __construct() {
		$this->weight = 70; //kg
		$this->speed = 10; //km/h
		$this->height = 1.2; //m
		$this->typeName = 'Hydra1';
	}
}

class MyHydra2 extends DefaultRobot
{
	/**
	 * MyHydra1 constructor.
	 */
	public function __construct() {
		$this->weight = 50; //kg
		$this->speed = 30; //km/h
		$this->height = 0.2; //m
		$this->typeName = 'Hydra2';
	}
}

/*
 * Class used to unite robots
 */
class UnionRobot extends DefaultRobot
{
	/**
	 * UnionRobot constructor.
	 */
	public function __construct() {
		$this->typeName = 'UnionRobot';
	}

	/**
	 * Function used to add robots to unite
	 *
	 * @param $robotsHandler
	 */
	public function addRobot($robotsHandler)
	{
		//check if it's array:
		$robotsToUnite = [];
		if (!is_array($robotsHandler))
		{
			//make it as array:
			$robotsToUnite[] = $robotsHandler;
		}
		else $robotsToUnite = $robotsHandler;
		//unite robots:
		foreach ($robotsToUnite as $currentRobot)
		{
			//proceed united weight:
			if (!isset($this->weight)) $this->weight = $currentRobot->weight;
			else
			{
				//weight should be summed:
				$this->weight += $currentRobot->weight;
			}
			//proceed united speed:
			if (!isset($this->speed)) $this->speed = $currentRobot->speed;
			else
			{
				//speed should be the lowest:
				$this->speed = $this->speed <= $currentRobot->speed ? $this->speed : $currentRobot->speed;
			}
			//proceed united height:
			if (!isset($this->height)) $this->height = $currentRobot->height;
			else
			{
				//height should be summed:
				$this->height += $currentRobot->height;
			}
		}
	}
}