<?php

namespace Robots;

class Factory
{
	protected $robotTypes;

	/**
	 * Factory constructor.
	 */
	public function __construct() {
		$this->robotTypes = [];
	}

	/**
	 * Function used to save types of the robots that could be produced
	 * @param $typeHandler
	 */
	public function addType($typeHandler)
	{
		//check if we got such robots type:
		if (!in_array($typeHandler, $this->robotTypes, true))
		{
			//add to current types:
			array_push($this->robotTypes, $typeHandler);
		}
	}

	/**
	 * Function used to create robots
	 *
	 * @param $robotTypeName
	 * @param $robotsAmount
	 *
	 * @return array|bool
	 */
	protected function createRobot($robotTypeName, $robotsAmount)
	{
		$producedRobots = [];
		foreach ($this->robotTypes as $currentRobotType)
		{
			if ($currentRobotType->typeName == $robotTypeName)
			{
				//generate array of robots:
				for($robotCount = 1; $robotCount <= $robotsAmount; $robotCount++)
				{
					$producedRobots[] = $currentRobotType;
				}
				return $producedRobots;
			}
		}
		return false;
	}

	/**
	 * Function used to create Hydra1 robots
	 *
	 * @param $robotsAmount
	 *
	 * @return array|bool
	 */
	public function createMyHydra1($robotsAmount)
	{
		return $this->createRobot('Hydra1', $robotsAmount);
	}

	/**
	 * Function used to create Hydra2 robots
	 *
	 * @param $robotsAmount
	 *
	 * @return array|bool
	 */
	public function createMyHydra2($robotsAmount)
	{
		return $this->createRobot('Hydra2', $robotsAmount);
	}

	/**
	 * Function used to create Union robots
	 *
	 * @param $robotsAmount
	 *
	 * @return array|bool
	 */
	public function createUnionRobot($robotsAmount)
	{
		return $this->createRobot('UnionRobot', $robotsAmount);
	}

	/**
	 * Used for debug
	 */
	public function showRobotTypes()
	{
		echo '<pre>';
		var_dump($this->robotTypes);
		echo '</pre>';
	}

}